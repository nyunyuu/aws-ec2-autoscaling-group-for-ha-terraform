## Demo App
In this project, Terraform is used to create an autoscaling group that can automatically scale our EC2 instances up or down based on demand. This will ensure that we always have the right number of instances running to meet our application’s needs.

Auto Scaling : To handle surge in traffic during the holiday season.
Security Group : To ensure that the instances in the Auto Scaling group are secure.
User Data : To launch an Apache web server.
Capacity : To ensure that the Auto Scaling group has the appropriate capacity.
Verification : To verify that everything is working correctly.
Reliability : To ensure that the infrastructure is reliable and can be easily managed.

## Architecture
![image info](HA-infra.jpg)