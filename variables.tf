variable "vpc_cidr" {
  type        = string
  default     = "10.0.0.0/16"
  description = "cidr block of the vpc"
}

variable "pub_subnet" {
  type        = list(string)
  default     = ["10.0.1.0/24", "10.0.2.0/24"]
  description = "list onf public subnets inside the VPC"
}

variable "pri_subnet" {
  type        = list(string)
  default     = ["10.0.10.0/24", "10.0.20.0/24"]
  description = "list onf private subnets inside the VPC"
}

variable "environment" {
  type    = string
  default = "LAB"
}

# Launch Template and ASG Variables
variable "instance_type" {
  description = "launch template EC2 instance type"
  type        = string
  default     = "t2.micro"
}

#This user data variable indicates that the script configures Apache on a server.
/*
variable "ec2_user_data" {
  description = "variable indicates that the script configures Apache on a server"
  type        = string
  default     = <<EOF
#!/bin/bash
# Install Apache on Ubuntu
sudo apt update -y
sudo apt install -y apache2
sudo cat > /var/www/html/index.html << EOF
<html>
<head>
  <title> Apache 2023 Terraform </title>
</head>
<body>
  <p> Welcome to my Apache WebPage powered by AWS EC2 Auto-Scaling Group using Terraform!
</body>
</html>
EOF
}*/
